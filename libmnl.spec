Name:libmnl
Version: 1.0.4
Release: 10
License: LGPLv2.1+
BuildRequires: gcc
URL: https://netfilter.org/projects/libmnl
Source0: https://netfilter.org/projects/libmnl/files/%{name}-%{version}.tar.bz2
Summary: A minimalistic user-space library oriented to netlink developers.

%description
libmnl is a minimalistic user-space library oriented to Netlink developers.
There are a lot of common tasks in parsing, validating, constructing of both
the Netlink header and TLVs that are repetitive and easy to get wrong.
This library aims to provide simple helpers that allows you to re-use code and
to avoid re-inventing the wheel.

%package devel
Summary: development library package for libmnl.
Requires: %{name} = %{version}-%{release}
Requires: %{name}%{_isa} = %{version}-%{release}
Provides: libmnl-static
Obsoletes: libmnl-static

%description devel
The devel package provide header files and dynamic libraries for libmnl.

%prep
%setup -q

%build
%configure --enable-static
make CFLAGS="%{optflags}" %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT
find examples -type d -name '.deps' -prune -exec rm -rf {} ';'
find $RPM_BUILD_ROOT -name '*.la' -delete
find examples '(' -name 'Makefile.am' ')' -delete
find examples '(' -name 'Makefile.in' ')' -delete
mv examples examples-%{_arch}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc README
%license COPYING
%{_libdir}/*.so.*

%files devel
%doc examples-%{_arch}
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0.4-10
- provides libmnl-static

* Mon Aug 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.4-9
- Package init
